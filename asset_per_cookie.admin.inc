<?php

/**
 * @file
 * Provides an administrative page for asset per cookie module.
 */

/**
 * Settings form for asset per cookie.
 */
function asset_per_cookie_settings() {
  $assetAmount = (int) variable_get('asset_per_cookie__amount', 5);
  $assetCount = 1;
  $form['asset_per_cookie__amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount of assets'),
    '#description' => t('Enter the amount of assets available to be rendered.<br>Default: 5.'),
    '#default_value' => $assetAmount,
  );
  while ($assetCount <= $assetAmount) {
    $form["tabs-$assetCount"] = array(
      '#type' => 'fieldset',
      '#title' => t("Asset") . " $assetCount",
      '#collapsible' => TRUE,
      '#collapsed' => ($assetCount == 1) ? FALSE : TRUE,
    );
    $form["tabs-$assetCount"]["asset_per_cookie__path__$assetCount"] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#default_value' => variable_get("asset_per_cookie__path__$assetCount"),
      '#description' => t('The path to the asset which you want to load when the cookie below matches.<br>The path can be both absolute, or relative.<br>Default: (empty).'),
      '#size' => 60,
    );
    $form["tabs-$assetCount"]["asset_per_cookie__cookie__$assetCount"] = array(
      '#type' => 'textfield',
      '#title' => t('Cookie'),
      '#default_value' => variable_get("asset_per_cookie__cookie__$assetCount"),
      '#description' => t('The cookie or cookies which should load the asset.<br>To use multiple, separate them by comma like: <br><em>fromFacebook,fromLinkedin,fromGoogle</em><br>Default: (empty).'),
      '#size' => 60,
    );
    $assetCount++;
  }
  $form['#submit'][] = 'asset_per_cookie_settings_submit';
  return system_settings_form($form);
}

/**
 * Submit handler for the settings form.
 */
function asset_per_cookie_settings_submit(&$form, &$form_state) {
  $assetAmount = (int) variable_get('asset_per_cookie__amount', 5);
  $savedAmount = (int) $form_state['values']['asset_per_cookie__amount'];
  if ($savedAmount < $assetAmount) {
    // Clean up the leftover asset and cookie variables.
    while ($savedAmount < $assetAmount) {
      variable_del("asset_per_cookie__path__$assetAmount");
      variable_del("asset_per_cookie__cookie__$assetAmount");
      $assetAmount--;
    }
  }
}
