# Asset per cookie

Asset per cookie is a module which lets you render
custom assets depending on the cookies available
for the user.

You can render custom CSS, Javascript and tracking
pixels.


## Installation:

  * Installation is like all normal drupal modules:
   * Extract the 'asset_per_cookie' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules/contrib).
   * Go to /admin/modules and enable the 'asset_per_cookie' module

## Dependencies:

  * The Asset per Cookie module has no dependencies to other modules.

## Conflicts/known issues:

  * There are no current conflicts nor known issues with this module.
  
## Configuration

  * Specify the amount of assets to process.
  * Specify the asset path, and the cookies which load it.
  * Enjoy.
