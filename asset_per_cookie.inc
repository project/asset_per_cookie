<?php

/**
 * @file
 * Helper functions for the Asset per Cookie module.
 *
 * Use module_load_include('inc', 'asset_per_cookie',
 * 'asset_per_cookie')
 * to use these functions within another module.
 *
 * @ingroup asset_per_cookie
 */

/**
 * Insert a cookie to the page.
 */
function _asset_per_cookie_insert_element($asset, $number) {
  if (strpos($asset, ".js") !== FALSE) {
    drupal_add_js($asset);
  }
  elseif (strpos($asset, ".css") !== FALSE) {
    drupal_add_css($asset);
  }
  elseif ((strpos($asset, ".jpg") !== FALSE)
  || (strpos($asset, ".png") !== FALSE)
  || (strpos($asset, ".php") !== FALSE)) {
    $element = array(
      '#type' => 'html_tag',
      '#tag' => 'img',
      '#attributes' => array(
        'src' => $asset,
        'style' => 'position:fixed;top:0;left:0;width:0;height:0;overflow:hidden;pointer-events:none;z-index:-10;',
      ),
    );
    drupal_add_html_head($element, "asset_per_cookie_$number");
  }
  else {
    drupal_set_message(t('The following asset could not be parsed: @asset. Please make sure to use one of the following extensions: .css, .js, .png, .jpg, .php.', array('@asset' => $asset)), 'warning');
  }
}
